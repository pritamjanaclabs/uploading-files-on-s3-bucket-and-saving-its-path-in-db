/**
 * Created by karan on 3/10/2015.
 */
var express = require('express');
var router = express.Router();
var commonFunc=require('./commonFunction');
var sendResponse=require('./sendResponse');
var async = require('async');

/* GET upload image. */
router.post('/userImageUpload', function(req, res) {
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, [req.files.user_image.name], callback);
        }],function(resultCallback) {
        var currentTime = new Date().getTime().toString();
        req.files.user_image.name = currentTime;

        //call imageUploadOnServer function oh common function for uploading a image.
        commonFunc.imageUploadOnServer(req.files.user_image, 'pjImages', function (returnResult) {
            if (returnResult === 0) {
                console.log("error");
                sendResponse.sendErrorMessage(constant.responseMessage.UPLOADING_ERROR, res);
            }
            else {
                //call insertImage function of common function file for insert a image url of s3 bucket into data base
                commonFunc.insertImage(res, returnResult, function (result) {
                    if (result == 0) {
                        console.log("error ache in inserting");
                        sendResponse.sendErrorMessage(constant.responseMessage.DB_INSERTING_ERROR, res);
                    }
                    else {
                        sendResponse.sendSuccessData(returnResult, res);
                        console.log(returnResult);
                    }
                });

            }
        });
    });
});

module.exports = router;