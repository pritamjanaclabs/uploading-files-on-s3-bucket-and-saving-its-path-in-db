/**
 * Created by karan on 3/10/2015.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'User Access' });
});

module.exports = router;