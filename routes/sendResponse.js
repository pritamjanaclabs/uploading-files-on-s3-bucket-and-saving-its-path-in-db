/**
 * Created by karan on 3/10/2015.
 */


exports.parameterMissingError = function (res) {

    var errResponse = {
        status: constant.responseStatus.PARAMETER_MISSING,
        message: constant.responseMessage.PARAMETER_MISSING,
        data: {}
    }
    sendData(errResponse,res);
};
exports.wrongMailId = function (res) {

    var successResponse = {
        status: constant.responseStatus.SHOW_ERROR_MESSAGE,
        message: 'Invalid Email Id',
        data: {}
    };

    sendData(successResponse,res);
};
exports.sendSuccessData = function (data,res) {

    var successResponse = {
        status: constant.responseStatus.SHOW_DATA,
        message: "",
        data: data
    };
    sendData(successResponse,res);
};
exports.successStatusMsg = function (res) {

    var successResponse = {
        status: constant.responseStatus.SHOW_MESSAGE,
        message: constant.responseMessage.SHOW_MESSAGE,
        data: {}
    };

    sendData(successResponse,res);
};
exports.sendErrorMessage = function (msg,res) {

    var errResponse = {
        status: constant.responseStatus.SHOW_ERROR_MESSAGE,
        message: msg,
        data: {}
    };
    sendData(errResponse,res);
};
function sendData(data,res)
{
    res.type('json');
    res.jsonp(data);
}