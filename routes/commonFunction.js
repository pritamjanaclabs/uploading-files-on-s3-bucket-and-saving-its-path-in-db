/**
 * Created by karan on 3/10/2015.
 */
var fs = require('fs');
var AWS = require('aws-sdk');
var extendPath = require('path')
var sendResponse=require('./sendResponse');
var awsConfig = config.get('awsSettings');

//definition of checkBlank that calling from user.js file
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}


//check passing parameter is blank or not
function checkBlank(arr) {

    var arrLength = arr.length;

    for (var i = 0; i < arrLength; i++) {
        if ((arr[i] == '')||(arr[i] == undefined)||(arr[i] == '(null)')) {
            return 1;
            break;
        }
    }
    return 0;
}

//image upload on s3 bucket with access key and also secretAccessKey
exports.imageUploadOnServer=function(img,folder,callback){

    var filename = img.name; // actual filename of file
    var path = img.path; //will be put into a temp directory
    var mimeType = img.type;
    /*console.log(filename);
     console.log(path);
     console.log(extention);*/

    fs.readFile(path, function (err, file_buffer) {
        if(err){
            return callback(0);
        }
        else{
            AWS.config.update({accessKeyId: awsConfig.awsAccessKey, secretAccessKey: awsConfig.awsSecretAccessKey});
            var s3bucket = new AWS.S3();
            var params = {Bucket: awsConfig.awsUserName, Key: folder + '/' + filename, Body: file_buffer, ACL: 'public-read', ContentType: mimeType};

            s3bucket.putObject(params, function(err, data) {
                if (err)
                {
                    console.log(err)
                    return callback(0);
                }
                else{
                    var url= awsConfig.awsURL+folder+'/'+filename
                    //console.log(url);
                    return callback(url);
                }
            });
        }

    });


}

//insert a image url into data base.
exports.insertImage=function(res,imgLink,callback){
    var sql = "INSERT INTO `img_s3bucket`(`img_url`) VALUES (?)";
    dbConnection.Query(res, sql, imgLink, function (userInsertResult) {
        if(userInsertResult.affectedRows==0)
            return callback(0);
        else
            return callback(1);
    });
}