process.env.NODE_ENV= 'test';
config=require('config');
constant = require('./routes/constant');
dbConnection = require('./routes/dbConnection');
var express = require('express');


var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var routes = require('./routes/index');
var upload = require('./routes/imgUpload');
var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'public')));
app.get('/', routes);
app.post('/userImageUpload', multipartMiddleware);
app.post('/userImageUpload', upload);

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});